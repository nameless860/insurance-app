class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.integer :payment_schedule
      t.float :current_price
      t.boolean :theft_coverage
      t.boolean :theft_coverage_upgrade
      t.boolean :deductible
      t.belongs_to :customer
      t.datetime :active_from

      t.timestamps
    end
  end
end
