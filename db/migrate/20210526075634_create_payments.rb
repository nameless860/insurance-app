class CreatePayments < ActiveRecord::Migration[5.2]
  def change
    create_table :payments do |t|
      t.float :amount
      t.belongs_to :customer
      t.belongs_to :contract
      t.integer :payment_type
      t.jsonb :description

      t.timestamps
    end
  end
end
