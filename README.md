# Database Design:

## The database have 4 tables:

### Customer: a customer can have many contracts and payments
- name
- email
- age

### Contract: a contract can have many payments and many price_logs
- payment_schedule: has one of these values [:yearly, :monthly, :quarterly]
- current_price: the amount of money that customers need to pay next time
- theft_coverage: a field that affects the contract price
- theft_coverage_upgrade: this field is used to simulate that the properties is changed
- deductable: a field that affects the contract price
- active_from: the date the contract is created

### Payment: will belong to one contract or one customer depending on payment_type
- amount: the amount of money need to pay
- payment_type: has one of these values: 'recurring' or 'additional_charge'
- description: the payment details description. This field has jsonb type and contains 2 keys:
  1. key: create_recurring_payment or create_additional_payment.
  2. contract: the contract details

### Pricelog: this table stores the old prices of a contract, will belong to one contract
- price
- created_at

# Classes:
### InsuranceApi: a service with the get_premium_price method is used to call API with contract's properties to get the contract price

### InsuranceCalculation: a service with 2 methods:
- calculate_recurring_amount: calculate the money must pay for a new cycle (monthly / quarterly / yearly)
- calculate_outstanding_amount: calculate the additional payment when the properties are changed (from the current date to the next payment day)

# Test cases: are inside spec/models folder
### There are 3 test files, each file is for one case of the following:
- monthly_spec
- quarterly_spec
- yearly_spec

### For each case, we will test:
- Correct recurring payment will be generated periodically (monthly or quarterly or yearly)
- When contract properties are changed, it should:
  * Generate additional payment
  * Update contract price
  * Log the old price

