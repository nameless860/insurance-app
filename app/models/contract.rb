class Contract < ApplicationRecord
  belongs_to :customer
  has_many :price_logs
  has_many :payments

  enum payment_schedule: [:yearly, :monthly, :quarterly]

  MONTHLY_CYCLE   = 12
  QUARTERLY_CYCLE = 4
  YEARLY_CYCLE    = 1

  def create_recurring_payment!
    # get premium
    premium_price = InsuranceApi.get_premium_price(self)
    # update if nil
    self.update(current_price: premium_price) unless self.current_price
    # calculator amount base on days used
    payment_amount = InsuranceCalculator.calculate_recurring_amount(self, premium_price)
    # create new payment for cycle
    # description for history log
    self.payments.create!(
      amount: payment_amount,
      customer_id: self.customer.id,
      payment_type: Payment::payment_types[:recurring],
      description: {
        key: 'create_recurring_payment', # for further using with I18n
        contract: self.to_json
      }
    )
  end

  def update_price!
    # call api to get premium price with current properties
    new_premium_price = InsuranceApi.get_premium_price(self)

    # price has changed
    if self.current_price != new_premium_price
      # calculate extra amount
      extra_amount_to_pay = InsuranceCalculator.calculate_outstanding_amount(self, new_premium_price)

      # create payment for outstanding
      self.payments.create!(
        amount: extra_amount_to_pay,
        customer_id: self.customer.id,
        payment_type: Payment::payment_types[:additional_charge],
        description: {
          key: 'create_additional_payment', # for further using with I18n
          contract: self.to_json
        }
      )

      # create history log for old price
      self.price_logs.create(price: self.current_price)

      # update new price for contract
      self.update(current_price: new_premium_price)
    end
  end

  def next_recurring_payment_date
    case self.payment_schedule
    when 'monthly'
      self.payments.present? ? self.payments.recurring.last.created_at.next_month : self.active_from.next_month
    when 'quarterly'
      self.payments.present? ? self.payments.recurring.last.created_at.months_since(3) : self.active_from.months_since(3)
    when 'yearly'
      self.payments.present? ? self.payments.recurring.last.created_at.next_year : self.active_from.next_year
    end
  end

  def last_recurring_payment_date
    self.payments.present? ? self.payments.recurring.last.created_at : self.active_from
  end

  def get_rate_payment_schedule
    case self.payment_schedule
    when 'monthly'
      MONTHLY_CYCLE
    when 'quarterly'
      QUARTERLY_CYCLE
    when 'yearly'
      YEARLY_CYCLE
    end
  end
end
