class Payment < ApplicationRecord
  belongs_to :customer
  belongs_to :contract

  enum payment_type: [:recurring, :additional_charge]
end
