class Customer < ApplicationRecord
  has_many :contracts
  has_many :payments
end
