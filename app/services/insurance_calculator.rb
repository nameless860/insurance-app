class InsuranceCalculator
  def self.calculate_recurring_amount(contract, premium_price)
    premium_price.to_f / contract.get_rate_payment_schedule
  end

  def self.calculate_outstanding_amount(contract, new_premium_price)
    # get next & last recurring payment
    next_recurring_payment_date  = contract.next_recurring_payment_date
    last_recurring_payment_date  = contract.last_recurring_payment_date
    # get number of days in current cycle
    num_days_in_current_cycle    = ((next_recurring_payment_date - last_recurring_payment_date) / 86400).to_i

    old_recurring_payment_amount = contract.current_price / contract.get_rate_payment_schedule
    new_recurring_payment_amount = new_premium_price / contract.get_rate_payment_schedule

    # calc days apply new price
    remaining_days_in_current_cycle = ((next_recurring_payment_date - Time.current) / 86400).to_i - 1

    # get rate per day
    old_daily_rate = old_recurring_payment_amount / num_days_in_current_cycle
    new_daily_rate = new_recurring_payment_amount / num_days_in_current_cycle

    outstanding_amount = (new_daily_rate - old_daily_rate) * remaining_days_in_current_cycle
  end
end