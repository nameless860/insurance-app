FactoryGirl.define do
  factory :contract do
    payment_schedule { Contract::payment_schedules[:yearly] }
    price { 600 }
    from { Date.parse('01-05-2021') }
    to { nil }
  end
end