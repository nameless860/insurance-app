require 'rails_helper'

RSpec.describe Contract, type: :model do
  describe "with monthly payment" do
    context "one month payment" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment with full-month" do
        before(:each) do
          # active contract and create invoice for May
          travel_to Date.parse("01-05-2021")
          # create active contract from this day
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:monthly],
            active_from: Date.parse("01-05-2021")
          )
          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(50.0)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end

      describe "should generate correct payment with full-month with other active from date" do
        before(:each) do
          # active contract and create invoice for May
          travel_to Date.parse("15-05-2021")
          # create active contract from this day
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:monthly],
            active_from: Date.parse("15-05-2021")
          )
          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(50.0)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end

      describe "should generate correct payment with full-month with recurring date on next month" do
        before(:each) do
          # active contract and create invoice for May
          travel_to Date.parse("15-05-2021")
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:monthly],
            active_from: Date.parse("15-05-2021")
          )
          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
          # travel to next month
          travel_to Date.parse("15-05-2021").next_month
          # create recurring payment
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do

          expect(@contract.payments.recurring.size).to eq(2)
          expect(@contract.payments.recurring.first.amount).to eq(50.0)
          expect(@contract.payments.recurring.first.created_at.strftime("%d-%m-%Y")).to eq("15-05-2021")
          expect(@contract.payments.recurring.last.amount).to eq(50.0)
          expect(@contract.payments.recurring.last.created_at.strftime("%d-%m-%Y")).to eq("15-06-2021")
        end
      end
    end

    context "property change" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment when property changed" do
        before(:each) do
          # 01 -> 25 => 25 days for current_price , 31 - 25 = > 6 days for new price
          # ((600.0 / 12) / 31) * 25 + ((700.0 / 12) / 31) * 6 = 51.61
          # ((600.0 / 12) / 31) * 31 = 50.0
          # extra_amount = 51.61 - 50.0 = 1.612

          # active contract and create invoice for May
          travel_to Date.parse("01-05-2021")

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:monthly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("01-05-2021")
          )

          @contract.create_recurring_payment!

          # on this day, user upgrade theft coverage upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true

          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(1.61)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end
      end

      describe "should generate correct payment when property changed with other active date" do
        before(:each) do
          # 15 -> 25 => 11 days for current_price , 31 - 11 = > 20 days for new price
          # ((600.0 / 12) / 31) * 11 + ((700.0 / 12) / 31) * 20 = 55.38
          # ((600.0 / 12) / 31) * 31 = 50.0
          # extra_amount = 55.38 - 50.0 = 5.37

          # active contract and create invoice for May
          travel_to Date.parse("15-05-2021")

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:monthly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("15-05-2021")
          )

          @contract.create_recurring_payment!

          # on this day, user upgrade theft coverage upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true

          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(5.38)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end
      end
    end
  end
end
