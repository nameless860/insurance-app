require 'rails_helper'

RSpec.describe Contract, type: :model do
  describe "with quarterly payment" do
    context "one quarterly" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment for contract with active from date" do

        before(:each) do
          # assum beginning-of-month generate invoice
          # active contract on this day and create invoice for 1st quarterly
          travel_to Date.parse("01-05-2021")

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:quarterly],
            active_from: Date.parse("01-05-2021")
          )

          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(150.0)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end

      describe "should generate correct payment for contract with other active from date" do

        before(:each) do
          # active contract on this day and create invoice for 1st quarterly
          travel_to Date.parse("15-05-2021").months_since(3)

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:quarterly],
            active_from: Date.parse("15-05-2021")
          )

          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(150.0)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end
    end

    context "property change" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment when property changed" do

        before(:each) do
          # 01 -> 25 => 25 days for current_price , 92 - 25 = > 67 days for new price
          # ((600.0 / 4) / 92) * 25 + ((700.0 / 4) / 92) * 67 = 168.21
          # ((600.0 / 4) / 92) * 92 = 150.0
          # extra_amount = 168.21  - 150.0 = 18.21

          # assum beginning-of-month generate invoice
          # active contract on this day and create invoice for 1st quarterly
          travel_to Date.parse("01-05-2021")

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:quarterly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("01-05-2021")
          )

          @contract.create_recurring_payment!

          # on this day, user upgrade property theft_coverage_upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true

          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(18.21)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end
      end

      describe "should generate correct payment when property changed" do

        before(:each) do
          # 15 -> 25 => 11 days for current_price , 92 - 11 = > 81 days for new price
          # ((600.0 / 4) / 92) * 11 + ((700.0 / 4) / 92) * 81 = 172.01
          # ((600.0 / 4) / 92) * 92 = 150.0
          # extra_amount = 172.01  - 150.0 = 22.01

          # assum beginning-of-month generate invoice
          # active contract on this day and create invoice for 1st quarterly
          travel_to Date.parse("15-05-2021")

          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:quarterly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("15-05-2021")
          )

          @contract.create_recurring_payment!

          # on this day, user upgrade property theft_coverage_upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true
          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(22.01)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end
      end
    end
  end
end
