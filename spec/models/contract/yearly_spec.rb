require 'rails_helper'

RSpec.describe Contract, type: :model do
  describe "with yearly payment" do
    context "one yearly have 90 days" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment for contract with active from date" do

        before(:each) do
          travel_to Date.parse("01-05-2021")
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:yearly],
            active_from: Date.parse("01-05-2021")
          )
          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(600.00)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end

      describe "should generate correct payment for contract with other active from date" do

        before(:each) do
          travel_to Date.parse("15-05-2021")
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:yearly],
            active_from: Date.parse("15-05-2021")
          )
          # get from api
          allow(InsuranceApi).to receive(:get_premium_price).and_return(600.0)
          # set price & payments
          @contract.create_recurring_payment!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(600.0)
        end

        it "should make correct payment" do
          recurring_payment = @contract.payments.recurring.last
          expect(recurring_payment.amount).to eq(600.00)
          expect(recurring_payment.payment_type).to eq('recurring')
          expect(recurring_payment.description.has_key?('key')).to be_truthy
          expect(recurring_payment.description.has_key?('contract')).to be_truthy
          expect(recurring_payment.description['key']).to eq('create_recurring_payment')
        end
      end
    end

    context "property change" do
      let(:customer) { FactoryGirl.create(:customer) }

      describe "should generate correct payment when property changed" do

        before(:each) do
          # 01 -> 25 => 25 days for current_price , 365 - 25 = > 340 days for new price
          # ((600.0) / 365) * 25 + ((700.0) / 365) * 340 = 693.15
          # ((600.0 / 365)) * 365 = 600.0
          # extra_amount = 693.15  - 600.0 = 93.15
          travel_to Date.parse("01-05-2021")
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:yearly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("01-05-2021")
          )
          # create invoice for May
          @contract.create_recurring_payment!

          # on this day, user upgrade property theft_coverage_upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true

          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end


        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(93.15)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end
      end

      describe "should generate correct payment when property changed with other active from date" do

        before(:each) do
          # 15 -> 25 => 11 days for current_price , 365 - 11 = > 354 days for new price
          # ((600.0) / 365) * 11 + ((700.0) / 365) * 354 = 696.99
          # ((600.0 / 365)) * 365 = 600.0
          # extra_amount = 696.99  - 600.0 = 96.99
          travel_to Date.parse("15-05-2021")
          @contract = customer.contracts.create(
            payment_schedule: Contract::payment_schedules[:yearly],
            current_price: 600.0, # current price for contract
            active_from: Date.parse("15-05-2021")
          )
          @contract.create_recurring_payment!

          # on this day, user upgrade property theft_coverage_upgrade
          travel_to Date.parse("25-05-2021")

          # property changed
          @contract.theft_coverage_upgrade = true
          # stub api new premium price
          allow(InsuranceApi).to receive(:get_premium_price).and_return(700.0)

          @contract.update_price!
        end

        it "should change the price of contract" do
          expect(@contract.current_price).to eq(700.0)
        end

        it "should make additional payment with extra amount" do
          payment = @contract.payments.additional_charge.last
          expect(payment.amount.round(2)).to eq(96.99)
          expect(payment.payment_type).to eq('additional_charge')
          expect(payment.description.has_key?('key')).to be_truthy
          expect(payment.description.has_key?('contract')).to be_truthy
          expect(payment.description['key']).to eq('create_additional_payment')
        end

        it "should log the old price of contract" do
          expect(@contract.price_logs.last.price).to eq(600.0)
        end
      end
    end
  end
end
