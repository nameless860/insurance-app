require 'rails_helper'

RSpec.describe Customer, type: :model do
  describe "testing" do
    context "create successfully customer" do
      let(:customer) { FactoryGirl.create(:customer) }

      it "create successfully customer" do
        expect(customer.id.present?).to eq(true)
      end

      it "create new contract for customer successfully" do
        contract = customer.contracts.create(
          payment_schedule: Contract::payment_schedules[:yearly],
          current_price: 600,
          theft_coverage: true,
          theft_coverage_upgrade: false,
          deductible: true,
        )
        expect(customer.contracts.present?).to eq(true)
        expect(customer.contracts.size).to eq(1)
        expect(customer.contracts.first).to eq(contract)
        expect(contract.customer).to eq(customer)
      end

      it "a contract has one customer successfully" do
        contract = Contract.create(
          payment_schedule: Contract::payment_schedules[:yearly],
          current_price: 600,
          theft_coverage: true,
          theft_coverage_upgrade: false,
          deductible: true,
        )
        contract.customer = customer
        expect(contract.customer).to eq(customer)
      end
    end
  end
end
